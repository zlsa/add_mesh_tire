
import bpy
import bmesh
import bpy.utils
import math

from bpy.props import (
        BoolProperty,
        EnumProperty,
        FloatProperty,
        IntProperty,
        StringProperty,
        FloatVectorProperty,
        )

bl_info = {
'name': 'Add Tire',
'description': 'Adds a tire',
'author': 'Jon Ross',
'version': (0, 1, 0),
'blender': (2, 79, 0),
'location': 'Add > Mesh > Tire',
'warning': 'Alpha',
'support': 'TESTING',
'category': '3D View',
'wiki_url': '<documentation URL>',
'tracker_url': '<bugtracker URL>'
}

# Creates the edge line for one half of the tire crown, from the center to the edge contact point.
def create_tire_crown(op):

  # The total distance from the nominal radius to the edges of the crown, in meters.
  crown_height = op.tire_crown_shape * op.tire_sidewall_height

  # The total width of half the crown, in meters.
  crown_width = (op.tire_width / 2)

  return create_bezier([(0, op.tire_radius), (crown_width, op.tire_radius), (crown_width, op.tire_radius - crown_height)], op.mesh_resolution_crown)

# Creates the edge line for the tire sidewall, from the crown to the bead start.
def create_tire_sidewall(op):

  # The total distance from the nominal radius to the edges of the crown, in meters.
  crown_height = op.tire_crown_shape * op.tire_sidewall_height

  # The total width of half the crown, in meters.
  crown_width = (op.tire_width / 2)

  wheel_radius = op.wheel_radius
  wheel_width = crown_width - op.wheel_inset

  return create_bezier([(crown_width, op.tire_radius - crown_height), (crown_width, wheel_radius), (wheel_width, wheel_radius)], op.mesh_resolution_sidewall)

def create_tire_bead(op):

  crown_width = (op.tire_width / 2)

  wheel_radius = op.wheel_radius
  wheel_width = crown_width - op.wheel_inset

  bead_width = op.bead_depth
  bead_height = op.bead_size

  verts = create_bezier([(wheel_width, wheel_radius), (wheel_width - bead_width, wheel_radius), (wheel_width - bead_width, wheel_radius - bead_height)], op.mesh_resolution_bead)

  verts.append((wheel_width - bead_width - op.tire_thickness, 0, wheel_radius - bead_height))
  verts.append((wheel_width - bead_width - op.tire_thickness, 0, wheel_radius))

  return verts

# `a` and `b` are expected to be `[x, y]` pairs.
def interpolate(a, b, interp):
  x = b[0] - a[0]
  y = b[1] - a[1]

  return [a[0] + (x * interp), a[1] + (y * interp)]

# Given 3 2D `points`, returns `resolution` 3D vertices.
def create_bezier(points, resolution):

  verts = []

  for i in range(resolution + 1):
    a = interpolate(points[0], points[1], (i / resolution))
    b = interpolate(points[1], points[2], (i / resolution))

    point = interpolate(a, b, (i / resolution))

    verts.append([point[0], 0, point[1]])

  return verts

def create_tire_mesh(op, context):
  # A single edge line, from one sidewall to the other.
  edge = []

  edge.extend(create_tire_crown(op))
  edge.extend(create_tire_sidewall(op))
  edge.extend(create_tire_bead(op))

  mesh = bpy.data.meshes.new("Tire")
  obj = bpy.data.objects.new("Tire", mesh)

  scene = context.scene
  scene.objects.link(obj)
  scene.objects.active = obj
  obj.select = True

  mesh = bpy.context.object.data
  bm = bmesh.new()

  verts = []

  for vert in edge:
    verts.append(bm.verts.new(vert))

  for i in range(len(verts)-1):
    bm.edges.new([verts[i], verts[i+1]])

  bmesh.ops.mirror(
    bm,
    geom=bm.edges,
    merge_dist=0.0001,
    axis=0
    )

  bmesh.ops.spin(
    bm,
    geom=bm.edges,
    angle=math.radians(360.0),
    steps=op.mesh_steps,
    axis=(1.0, 0.0, 0.0),
    cent=(0.0, 0.0, 0.0))

  bmesh.ops.remove_doubles(
    bm,
    verts=bm.verts,
    dist=0.0001)
  bmesh.ops.recalc_face_normals(
    bm,
    faces=bm.faces)

  for f in bm.faces:
    f.smooth = True

  bm.to_mesh(mesh)  
  bm.free()


class AddTire(bpy.types.Operator):
  bl_idname = 'mesh.tire_add'
  bl_label = 'Add Tire'
  bl_description = 'Add a tire mesh.'
  bl_options = {'REGISTER', 'UNDO', 'PRESET'}
 
  use_tire_spec = BoolProperty(
    name='Use Tire Specification',
    description='Use "P205/65R15" measurements instead of unit sizes',
    default=True
    )

  # Unit sizes

  tire_radius = FloatProperty(
    name='Radius',
    description='Radius of tire',
    unit='LENGTH',
    min=0.01,
    max=8,
    default=0.5
    )

  tire_width = FloatProperty(
    name='Width',
    description='Width of tire from sidewall to sidewall',
    unit='LENGTH',
    min=0.01,
    max=4,
    default=0.25
    )

  tire_sidewall_height = FloatProperty(
    name='Sidewall Height',
    description='Height of sidewall',
    unit='LENGTH',
    min=0.01,
    max=4,
    default=0.18
    )

  wheel_radius = FloatProperty(
    name='Wheel Radius',
    description='Radius of wheel',
    unit='LENGTH',
    min=0.01,
    max=8,
    default=0.35
    )

  wheel_inset = FloatProperty(
    name='Wheel Inset',
    description='The inset from the sidewall to the bead',
    unit='LENGTH',
    min=0.0,
    max=8,
    default=0.005
    )

  bead_depth = FloatProperty(
    name='Bead Depth',
    description='The horizontal depth of the bead',
    unit='LENGTH',
    min=0.0,
    max=8,
    default=0.015
    )

  bead_size = FloatProperty(
    name='Bead Size',
    description='The vertical size of the bead',
    unit='LENGTH',
    min=0.0,
    max=8,
    default=0.015
    )

  tire_thickness = FloatProperty(
    name='Thickness',
    description='Thickness of tire at the bead',
    unit='LENGTH',
    min=0.01,
    max=8,
    default=0.01
    )

  # Tire spec

  tire_spec_type = EnumProperty(
    name='Tire Type',
    description='The type of tire (P for American passenger tires, LT for American light-truck tires, or E for Euro tires.)',
    items = [
      ('p', 'P', 'American passenger'),
      ('lt', 'LT', 'American light truck'),
      ('st', 'ST', 'American special trailer'),
      ('e', 'E', 'Euro'),
    ],
    default='p'
    )

  tire_spec_width = IntProperty(
    name='Width',
    description='Width of the tire, in millimeters',
    min=1,
    max=4000,
    default=215
    )

  tire_spec_ratio = FloatProperty(
    name='Ratio',
    description='Ratio between the tire width and sidewall height',
    min=0,
    max=1000,
    default=55
    )

  tire_spec_construction = EnumProperty(
    name='Tire Construction',
    description='The construction of the tire (R for radial)',
    items = [
      ('r', 'R', 'Radial')
    ],
    default='r'
    )

  tire_spec_wheel_diameter = FloatProperty(
    name='Wheel Diameter',
    description='The diameter of the wheel rim, in inches',
    min=0,
    max=1000,
    default=16
    )

  # Tire misc

  tire_crown_shape = FloatProperty(
    name='Tire Crown Shape',
    description='Roundness of the tire crown',
    min=0.0,
    max=1.0,
    default=0.08
    )

  # Tire mesh

  mesh_steps = IntProperty(
    name='Steps',
    description='Number of vertices around the radius of the tire',
    min=3,
    max=512,
    default=64,
    )

  mesh_resolution_crown = IntProperty(
    name='Crown Resolution',
    description='Number of vertices used on one side of the tire crown',
    min=2,
    max=256,
    default=8,
    )

  mesh_resolution_sidewall = IntProperty(
    name='Sidewall Resolution',
    description='Number of vertices used on the tire sidewall',
    min=2,
    max=256,
    default=4,
    )

  mesh_resolution_bead = IntProperty(
    name='Bead Resolution',
    description='Number of vertices used on the tire bead',
    min=2,
    max=64,
    default=3,
    )

  def draw(self, context):
    scene = context.scene
    layout = self.layout

    self.draw_tire_resolution(context, layout)

    layout.prop(self, 'use_tire_spec', toggle=True)

    box = layout.box()

    if self.use_tire_spec:
      self.draw_tire_spec(context, box)
    else:
      self.draw_tire_unit(context, box)

    self.draw_bead(context, layout)

    self.draw_tire_shape(context, layout)

  def draw_tire_resolution(self, context, layout):
    row = layout.row(align=True)

    row.prop(self, 'mesh_steps')

    layout.label('Resolution:')

    row = layout.row(align=True)

    split = row.split(align=True)
    split.prop(self, 'mesh_resolution_crown', text='Crown')
    split.prop(self, 'mesh_resolution_sidewall', text='Sidewall')
    split.prop(self, 'mesh_resolution_bead', text='Bead')

  def draw_tire_spec(self, context, layout):
    row = layout.row(align=True)

    split = row.split(align=True)
    split.prop(self, 'tire_spec_type', text='')

    split.prop(self, 'tire_spec_width', text='')
    split.prop(self, 'tire_spec_ratio', text='')

    split.prop(self, 'tire_spec_construction', text='')

    split.prop(self, 'tire_spec_wheel_diameter', text='')

    row = layout.row(align=True)
    split = row.split(align=True)
    split.prop(self, 'wheel_inset')

  def draw_tire_unit(self, context, layout):
    row = layout.row(align=True)
    split = row.split(align=True)
    split.prop(self, 'tire_radius')
    split.prop(self, 'tire_width')

    row = layout.row(align=True)
    split = row.split(align=True)
    split.prop(self, 'wheel_radius')
    split.prop(self, 'wheel_inset')

  def draw_tire_shape(self, context, layout):
    row = layout.row(align=True)

    split = row.split(align=True)
    split.prop(self, 'tire_thickness')
    split.prop(self, 'tire_crown_shape')

  def draw_bead(self, context, layout):
    row = layout.row(align=True)

    split = row.split(align=True)
    split.prop(self, 'bead_depth')
    split.prop(self, 'bead_size')

  # Converts the `tire_spec_*` variables to the unit variables.
  def update_from_tire_spec(self):
    # `tire_spec_width` is in mm, so we need to convert it to meters.
    self.tire_width = bpy.utils.units.to_value(bpy.utils.units.systems.METRIC, bpy.utils.units.categories.LENGTH, str(self.tire_spec_width) + 'mm')

    # Calculate wheel diameter in meters
    self.wheel_radius = bpy.utils.units.to_value(bpy.utils.units.systems.METRIC, bpy.utils.units.categories.LENGTH, str(self.tire_spec_wheel_diameter) + 'in') / 2

    # TODO: handle millimeter diameters for `tire_spec_ratio`.
    self.tire_radius = (self.tire_width * (self.tire_spec_ratio / 100)) + self.wheel_radius

  # Called before mesh creation. Used to disable undo and deselect all objects.
  def pre_execute(self, context):
    self.undo = bpy.context.user_preferences.edit.use_global_undo
    bpy.context.user_preferences.edit.use_global_undo = False

    if bpy.ops.object.select_all.poll():
      bpy.ops.object.select_all(action='DESELECT')

  # Called after mesh creation. Restores global undo state.
  def post_execute(self, context):
    context.user_preferences.edit.use_global_undo = self.undo

  # Creates the tire mesh.
  def create_mesh(self, context):
    create_tire_mesh(self, context)

  def execute(self, context):
    self.pre_execute(context)

    try:
      if self.use_tire_spec:
        self.update_from_tire_spec()

      self.create_mesh(context)
    except Exception as e:
      print('There was a tewwible ewwow')
      print(e)
    finally:
      self.post_execute(context)

    return {'FINISHED'}

def menu_func_tire(self, context):
  self.layout.operator(AddTire.bl_idname, text="Tire", icon="MESH_CIRCLE")

def register():
  bpy.utils.register_class(AddTire)
  bpy.types.INFO_MT_mesh_add.append(menu_func_tire)

def unregister():
  bpy.utils.unregister_class(AddTire)
  bpy.types.INFO_MT_mesh_add.remove(menu_func_tire)
