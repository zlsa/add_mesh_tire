
# Add Tire

This Blender add-on creates a tire mesh. The dimensions can be entered
manually, or via a tire specification (such as `P225/45R15`). The mesh
generation itself uses Bézier curves (in Python, not as a Blender object), and
as such, it is possible to generate bad mesh geometry.

![some tires yo](images/tire-assortment.jpg)